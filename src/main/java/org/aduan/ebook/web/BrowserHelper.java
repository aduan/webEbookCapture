package org.aduan.ebook.web;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by aduan on 15-7-26.
 */
public class BrowserHelper {
    public static WebDriver getChromeDriver() {
        //String driverDir = System.getProperty("chrome.driver.dir");
        System.setProperty("webdriver.chrome.driver", System.getProperty("chrome.driver"));
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
        return webDriver;
    }
}
