package org.aduan.ebook.web;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by aduan on 15-7-26.
 */
public class BrowserHelperTest {
    final Logger logger = LoggerFactory.getLogger(getClass());
    @Test
    public void testBrowser() throws Exception {
        WebDriver driver = BrowserHelper.getChromeDriver();
        driver.get("http://www.duokan.com");
        WebElement readLink = driver.findElement(By.xpath("//a[@href='/reader?id=af273508f4a34ed08c1fdc43c5a8891a']"));
        try {
            driver.quit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testParseHtml() {
        String destLink = "<a href=\"/r/%E7%B2%BE%E5%93%81%E6%8E%A8%E8%8D%90\" hidefocus=\"hidefocus\">" +
                "精品<span class=\"u-dot\">·</span>免费</a>";
        Document doc = Jsoup.parse("");
//        driver.findElement(By)
        Element link = doc.html(destLink).getAllElements().get(0);
        logger.info("{}", link.select("a").get(0));
        for(Attribute attr : link.select("a").get(0).attributes()) {
            logger.info("{}: {}", attr.getKey(), attr.getValue());
        }
    }
}
